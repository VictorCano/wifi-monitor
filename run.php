<?php

ob_implicit_flush(true);

$cmd = "sudo tshark -i wlan0 -I -f 'broadcast' -T fields -e frame.number -e frame.time_epoch -e wlan.sa -e radiotap.dbm_antsignal -e wlan.bssid -e wlan.fc.type -e wlan.fc.subtype -e wlan_mgt.ssid";

$file = fopen('./files/monitor-' . time() . '.txt', 'w');

$descriptorspec = array(
    0 => array("pipe", "r"), // stdin is a pipe that the child will read from
    1 => array("pipe", "w"), // stdout is a pipe that the child will write to
    2 => array("pipe", "w"), // stderr is a pipe that the child will write to
);
flush();
$process = proc_open($cmd, $descriptorspec, $pipes, realpath('./'), array());
if (is_resource($process)) {
    while ($s = fgets($pipes[1])) {
        $analyse = explode("	", $s);

        list($number, $timestamp, $mac, $signal, $destSsid, $type, $subtype, $ssid) = $analyse;
        //if ($subtype == 8) {
        $line = implode(";", $analyse) . "\n";
        fwrite($file, $line);
        //}

        flush();

    }
}

fclose($file);
